/*
 * AD9850.h
 *
 *  Created on: 02-02-2013
 *      Author: Admin
 */

#ifndef AD9850_H_
#define AD9850_H_

#include <stdint.h>
#include <avr/io.h>

typedef enum {
	MUL_x2 = 200,
	MUL_x2p5 = 250,
	MUL_x3 = 300,
	MUL_x3p33 = 333,
	MUL_x4 = 400,
	MUL_x5 = 500
} pllMuli;

typedef enum {
	sLow,
	sHigh,
	sHiZ
} triState;

#define DDS_DDR_DATA DDRE
#define DDS_PORT_DATA PORTE
#define DDS_PIN_DATA PE4

#define DDS_DDR_CLK DDRE
#define DDS_PORT_CLK PORTE
#define DDS_PIN_CLK PE5

#define DDS_DDR_FQ DDRG
#define DDS_PORT_FQ PORTG
#define DDS_PIN_FQ PG5

#define DDS_DDR_S0 DDRE
#define DDS_PORT_S0 PORTE
#define DDS_PIN_S0 PE3

#define DDS_DDR_S1 DDRH
#define DDS_PORT_S1 PORTH
#define DDS_PIN_S1 PH3


class AD9850 {
	static const uint8_t ARDUINO_PIN_S0 = 5;
	static const uint8_t ARDUINO_PIN_S1 = 6;

	static const uint32_t DDS_CLK = 125000000;
	void initOutputs();
	inline void writeBit(uint32_t bit, uint8_t end);
	inline void setPinTs(uint8_t pin, triState value);
	//void setPinPort(volatile uint8_t *port, uint8_t pin, uint8_t bit);
public:
	AD9850();
	uint32_t getFreqParam(uint32_t freq);
	void setFreq(uint32_t freq, uint8_t control);
	void setPll(pllMuli multi);
};

#endif /* AD9850_H_ */
