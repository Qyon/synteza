/*
 * AD9850.cpp
 *
 *  Created on: 02-02-2013
 *      Author: Admin
 */

#include "AD9850.h"
#include <Arduino.h>
#include <avr/io.h>
#include <avr/delay.h>

#define SET_PIN_PORT(port, pin, value) \
	if ((volatile uint8_t)value) \
		(port)|=_BV(pin); \
	else \
		(port)&=~_BV(pin);

#define SET_PIN_DIR(ddr, pin, dir) \
	if ((volatile uint8_t)dir == OUTPUT) \
		(ddr)|=_BV(pin); \
	else \
		(ddr)&=~_BV(pin);


void AD9850::initOutputs() {
	//digitalWrite(DDS_PIN_CLK, LOW);
	SET_PIN_PORT(DDS_PORT_CLK, DDS_PIN_CLK, LOW);
	//digitalWrite(DDS_PIN_DATA, LOW);
	SET_PIN_PORT(DDS_PORT_DATA, DDS_PIN_DATA, LOW);

	//digitalWrite(DDS_PIN_FQ, LOW);
	SET_PIN_PORT(DDS_PORT_FQ, DDS_PIN_FQ, LOW);
	//digitalWrite(DDS_PIN_S0, LOW);
	SET_PIN_PORT(DDS_PORT_S0, DDS_PIN_S0, LOW);
	//digitalWrite(DDS_PIN_S1, LOW);
	SET_PIN_PORT(DDS_PORT_S1, DDS_PIN_S1, LOW);

	SET_PIN_DIR(DDS_DDR_CLK, DDS_PIN_CLK, OUTPUT);
	SET_PIN_DIR(DDS_DDR_DATA, DDS_PIN_DATA, OUTPUT);
	SET_PIN_DIR(DDS_DDR_FQ, DDS_PIN_FQ, OUTPUT);
	SET_PIN_DIR(DDS_DDR_S0, DDS_PIN_S0, OUTPUT);
	SET_PIN_DIR(DDS_DDR_S1, DDS_PIN_S1, OUTPUT);

}

AD9850::AD9850() {
	initOutputs();
	this->setFreq(500000,0);
}

uint32_t AD9850::getFreqParam(uint32_t freq){
	return freq * 4294967296LL / this->DDS_CLK;
}

void AD9850::setFreq(uint32_t freq, uint8_t control){
	uint8_t i =0;
	freq = this->getFreqParam(freq);
	for(i=0;i<32;i++){
			this->writeBit((freq & (1UL<<i)), false);
	}
	for(i=0;i<8;i++){
			this->writeBit((control & (1<<i)) != 0, (i == 7));
	}
}

void AD9850::writeBit(uint32_t bit, uint8_t end) {
	if (bit > 0){
		bit = 1;
	} else {
		bit = 0;
	}
	//digitalWrite(DDS_PIN_DATA, bit);
	SET_PIN_PORT(DDS_PORT_DATA, DDS_PIN_DATA, bit);
	_delay_us(1);
	//digitalWrite(this->PIN_CLK, HIGH);
	SET_PIN_PORT(DDS_PORT_CLK, DDS_PIN_CLK, HIGH);
	_delay_us(1);
	//digitalWrite(this->PIN_CLK, LOW);
	SET_PIN_PORT(DDS_PORT_CLK, DDS_PIN_CLK, LOW);
	//digitalWrite(this->PIN_DATA, LOW);
	SET_PIN_PORT(DDS_PORT_DATA, DDS_PIN_DATA, LOW);
	if (end) {
		//digitalWrite(this->PIN_FQ, HIGH);
		SET_PIN_PORT(DDS_PORT_FQ, DDS_PIN_FQ, HIGH);
		_delay_us(2);
		//digitalWrite(this->PIN_FQ, LOW);
		SET_PIN_PORT(DDS_PORT_FQ, DDS_PIN_FQ, LOW);
	}

}

void AD9850::setPll(pllMuli multi) {
	switch(multi){
	case MUL_x2:
		this->setPinTs(this->ARDUINO_PIN_S0, sLow);
		this->setPinTs(this->ARDUINO_PIN_S1, sLow);
		break;
	case MUL_x2p5:
		this->setPinTs(this->ARDUINO_PIN_S0, sHigh);
		this->setPinTs(this->ARDUINO_PIN_S1, sHigh);
		break;
	case MUL_x3:
		this->setPinTs(this->ARDUINO_PIN_S0, sLow);
		this->setPinTs(this->ARDUINO_PIN_S1, sHiZ);
		break;
	case MUL_x3p33:
		this->setPinTs(this->ARDUINO_PIN_S0, sHigh);
		this->setPinTs(this->ARDUINO_PIN_S1, sHiZ);
		break;
	case MUL_x4:
		this->setPinTs(this->ARDUINO_PIN_S0, sLow);
		this->setPinTs(this->ARDUINO_PIN_S1, sHigh);
		break;
	case MUL_x5:
		this->setPinTs(this->ARDUINO_PIN_S0, sHigh);
		this->setPinTs(this->ARDUINO_PIN_S1, sLow);
		break;
	}
}

void AD9850::setPinTs(uint8_t pin, triState value){
	if (value == sLow){
		pinMode(pin, OUTPUT);
		digitalWrite(pin, LOW);
	} else if (value == sHigh){
		pinMode(pin, OUTPUT);
		digitalWrite(pin, HIGH);
	} else {
		pinMode(pin, INPUT);
		digitalWrite(pin, LOW);
	}
}
