/*
 * Cli.cpp
 *
 *  Created on: 03-02-2013
 *      Author: Admin
 */

#include "Cli.h"
#include <stdio.h>
#include "Arduino.h"
#include "AD9850.h"

Cli::Cli() {
	// TODO Auto-generated constructor stub
	this->frequency = 500000;
	this->pll = MUL_x4;
	ad9850.setPll(this->pll);
	ad9850.setFreq(this->frequency,0);
}

char * Cli::processData(char * buffer) {
	char * result = 0;
	switch(buffer[0]){
	case 'f':
		result = this->functionFrequency(buffer + 1);
		break;
	case 'p':
		result = this->functionPll(buffer + 1);
		break;
	case 'i':
		result = this->functionIncrease(*(buffer + 1) - '0');
		break;
	case 'd':
		result = this->functionDecrease(*(buffer + 1) - '0');
		break;
	}

	return result;
}

inline uint32_t Cli::power10(uint8_t i) {
	uint32_t x = 0;
	if (i == 0) {
		x = 1;
	} else {
		x = 10;
		while (--i) {
			x *= 10;
		}
	}
	return x;
}

char * Cli::functionIncrease(uint8_t i) {
	return this->realSetFrequency(this->frequency + power10(i));
}

char * Cli::functionDecrease(uint8_t i){
	return this->realSetFrequency(this->frequency - power10(i));
}


char * Cli::functionFrequency(char * buffer){
	if (buffer[0] == 0 || buffer[0] == '\n' || buffer[0] == '\r'){
		return this->printFrequency();
	} else {
		return this->setFrequency(buffer);
	}
}
char * Cli::functionPll(char * buffer){
	if (buffer[0] == 0 || buffer[0] == '\n'|| buffer[0] == '\r'){
		return this->printPll();
	} else {
		return this->setPll(buffer);
	}
}

char * Cli::printFrequency(){
	return 0;
	vsnprintf(this->tmp, 32, "F%lu\r\n", &(this->frequency));
	return this->tmp;
}

char * Cli::realSetFrequency(uint32_t newFrequency) {
	if (newFrequency >= 100000 && newFrequency <= 30000000) {
		this->frequency = newFrequency;
	} else {
		return "Invalid\r\n";
	}
	ad9850.setFreq(this->frequency, 0);
	return this->printFrequency();
}

char * Cli::setFrequency(char * buffer){
	unsigned long newFrequency = 0;
	sscanf(buffer, "%lu", &newFrequency);
	return this->realSetFrequency(newFrequency);
}

char * Cli::printPll(){
	vsnprintf(this->tmp, 32, "P%u\r\n", &(this->pll));
	return this->tmp;
}

char * Cli::setPll(char * buffer){
	unsigned int newPll = 0;
	sscanf(buffer, "%u", &newPll);
	if (newPll) {
		switch(newPll) {
		case MUL_x2:
		case MUL_x2p5:
		case MUL_x3:
		case MUL_x3p33:
		case MUL_x4:
		case MUL_x5:
			this->pll = (pllMuli)newPll;
		break;
		}
	}
	ad9850.setPll(this->pll);
	return this->printPll();
}

