/*
 * AstroDrive.c
 *
 *  Created on: 11-11-2012
 *      Author: Admin
 */



#include <Arduino.h>
#include "AD9850.h"
#include "Cli.h"

Cli cli;
// The setup() method runs once, when the sketch starts
void setup()   {
	//pinMode(LED_BUILTIN, OUTPUT);
  // initialize the digital pin as an output:
  Serial.setTimeout(100);
  Serial.begin( 9600 );
}
#define BUFFER_LEN 32
char buffer[BUFFER_LEN];
// the loop() method runs over and over again,
// as long as the Arduino has power
void loop()
{
    // send data only when you receive data:
    if (Serial.available() > 0) {
            size_t recived = Serial.readBytesUntil('\r', buffer, BUFFER_LEN-1);
            buffer[recived] = '\0';
            //Serial.print('>');
            //Serial.print(buffer);
            //Serial.println('.');
            char * result = cli.processData(buffer);
            if (result) {
            	//Serial.write(result);
            }
            Serial.println('.');
    }
}


int main(void) {
  init();
  setup();

  while(true) {
    loop();
  }
}
