/*
 * Cli.h
 *
 *  Created on: 03-02-2013
 *      Author: Admin
 */

#ifndef CLI_H_
#define CLI_H_

#include "AD9850.h"
#include <stdint.h>

class Cli {
	char * functionFrequency(char * buffer);
	char * functionPll(char * buffer);
	char * printFrequency();
	char * setFrequency(char * buffer);
	char * printPll();
	char * setPll(char * buffer);
	char * functionIncrease(uint8_t i);
	char * functionDecrease(uint8_t i);
	char * realSetFrequency(uint32_t newFrequency);
	uint32_t power10(uint8_t i);
	uint32_t frequency;
	pllMuli pll;
	char tmp[32];
	AD9850 ad9850;
public:
	Cli();
	char * processData(char * buffer);
};

#endif /* CLI_H_ */
